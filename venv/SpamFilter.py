import urllib.request
from bs4 import BeautifulSoup
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.classify import NaiveBayesClassifier
import os
import random

rootdir = "/root/Documents/IntelligentSystems/SpamFilter/venv/Enron"

ham_list = []
spam_list = []
# Set of stopwords in the english dictionary
stop_words = set(stopwords.words('english'))


# Create word-label dictionary for NTLK classifier
def create_word_features(words):
    my_dict = dict([(word, True) for word in words])
    return my_dict


# File parsing
for directories, subdirs, files in os.walk(rootdir):
    if os.path.split(directories)[1] == 'ham':
        for filename in files:
            with open(os.path.join(directories, filename), encoding="latin-1") as f:
                data = f.read()
                # Break data into words
                words = word_tokenize(data)

                # Remove the stopwords from the input data
                filtered_words = []
                for w in words:
                    if w not in stop_words:
                        filtered_words.append(w)

                ham_list.append((create_word_features(filtered_words), "ham"))

for directories, subdirs, files in os.walk(rootdir):
    if os.path.split(directories)[1] == 'spam':
        for filename in files:
            with open(os.path.join(directories, filename), encoding="latin-1") as f:
                data = f.read()
                # Break data into words
                words = word_tokenize(data)

                # Remove the stopwords from the input data
                filtered_words = []
                for w in words:
                    if w not in stop_words:
                        filtered_words.append(w)

                spam_list.append((create_word_features(filtered_words), "spam"))

# Combine ham and spam list and shuffle to randomize
combined_list = ham_list + spam_list
random.shuffle(combined_list)

# Split data into training and test set
training_part = int(len(combined_list) * .7)
training_set = combined_list[:training_part]
test_set = combined_list[training_part:]
print(len(combined_list))
print(len(training_set))
print(len(test_set))

# Create Bayes classifier
classifier = NaiveBayesClassifier.train(training_set)
# Compute accuracy against test set
accuracy = nltk.classify.util.accuracy(classifier, test_set)
print("Accuracy is: ", accuracy * 100)
classifier.show_most_informative_features()

# Classify example email
msg1 = '''Can you please send me the email containing the requested stuff that I requested?'''

msg2 = '''This is your LAST chance to enter in the competition. Sign Up NOW!'''

msg3 = '''Victor is my dear colleague '''

words = word_tokenize(msg1)
features = create_word_features(words)
print("Message 1 is :", classifier.classify(features))

words = word_tokenize(msg2)
features = create_word_features(words)
print("Message 2 is :", classifier.classify(features))

words = word_tokenize(msg3)
features = create_word_features(words)
print("Message 3 is : ", classifier.classify(features))
